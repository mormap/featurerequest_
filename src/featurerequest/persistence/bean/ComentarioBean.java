package featurerequest.persistence.bean;

import java.io.Serializable;
import java.sql.Timestamp;

public class ComentarioBean implements Serializable {
	private long id;
	private String texto;
	private Timestamp fechaEnvio;
	private PeticionBean peticion;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the texto
	 */
	public String getTexto() {
		return texto;
	}

	/**
	 * @param texto
	 *            the texto to set
	 */
	public void setTexto(String texto) {
		this.texto = texto;
	}

	/**
	 * @return the fechaEnvio
	 */
	public Timestamp getFechaEnvio() {
		return fechaEnvio;
	}

	/**
	 * @param fechaEnvio
	 *            the fechaEnvio to set
	 */
	public void setFechaEnvio(Timestamp fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	/**
	 * @return the peticion
	 */
	public PeticionBean getPeticion() {
		return peticion;
	}

	/**
	 * @param peticion the peticion to set
	 */
	public void setPeticion(PeticionBean peticion) {
		this.peticion = peticion;
	}

}