package featurerequest.persistence.bean;

import java.util.Set;

public class UsuarioDesarrolladorBean extends UsuarioBean {

	private Set<ProductoBean> productos;

	/**
	 * @return the productos
	 */
	public Set<ProductoBean> getProductos() {
		return productos;
	}

	/**
	 * @param productos the productos to set
	 */
	public void setProductos(Set<ProductoBean> productos) {
		this.productos = productos;
	}

}