package featurerequest.persistence.bean;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

public class PeticionBean implements Serializable {
	private long id;
	private UsuarioBean solicitante;
	private Set<ComentarioBean> comentarios;
	private Categoria categoria;
	private Estado estado;
	private String titulo;
	private String descripcion;
	private Timestamp fechaCreacion;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the solicitante
	 */
	public UsuarioBean getSolicitante() {
		return solicitante;
	}

	/**
	 * @param solicitante the solicitante to set
	 */
	public void setSolicitante(UsuarioBean solicitante) {
		this.solicitante = solicitante;
	}

	/**
	 * @return the comentarios
	 */
	public Set<ComentarioBean> getComentarios() {
		return comentarios;
	}

	/**
	 * @param comentarios the comentarios to set
	 */
	public void setComentarios(Set<ComentarioBean> comentarios) {
		this.comentarios = comentarios;
	}

	/**
	 * @return the categoria
	 */
	public Categoria getCategoria() {
		return categoria;
	}

	/**
	 * @param categoria the categoria to set
	 */
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	/**
	 * @return the estado
	 */
	public Estado getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	/**
	 * @return the titulo
	 */
	public String getTitulo() {
		return titulo;
	}

	/**
	 * @param titulo the titulo to set
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the fechaCreacion
	 */
	public Timestamp getFechaCreacion() {
		return fechaCreacion;
	}

	/**
	 * @param fechaCreacion the fechaCreacion to set
	 */
	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public enum Categoria {
		NUEVA_CATEGORIA, BUG
	}

	public enum Estado {
		ABIERTA, RESUELTA, DESCARTADA, REVISION
	}
}