package featurerequest.persistence.bean;

import java.util.Set;

public class UsuarioSolicitanteBean extends UsuarioBean {

	private Set<PeticionBean> peticiones;

	/**
	 * @return the peticiones
	 */
	public Set<PeticionBean> getPeticiones() {
		return peticiones;
	}

	/**
	 * @param peticiones the peticiones to set
	 */
	public void setPeticiones(Set<PeticionBean> peticiones) {
		this.peticiones = peticiones;
	}



}