package featurerequest.persistence.bean;

import java.io.Serializable;
import java.util.Set;

public class ProductoBean implements Serializable {
	private long id;
	private UsuarioBean desarrollador;
	private Set<PeticionBean> peticiones;
	private String nombre;
	private String version;
	private String descripcion;
	
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the desarrollador
	 */
	public UsuarioBean getDesarrollador() {
		return desarrollador;
	}
	/**
	 * @param desarrollador the desarrollador to set
	 */
	public void setDesarrollador(UsuarioBean desarrollador) {
		this.desarrollador = desarrollador;
	}
	/**
	 * @return the peticiones
	 */
	public Set<PeticionBean> getPeticiones() {
		return peticiones;
	}
	/**
	 * @param peticiones the peticiones to set
	 */
	public void setPeticiones(Set<PeticionBean> peticiones) {
		this.peticiones = peticiones;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}
	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



}