package featurerequest.persistence.bean;

public enum Estado {
	ABIERTA, RESUELTA, DESCARTADA, REVISION
}
