package featurerequest.persistence.dao;

import java.util.List;


import featurerequest.persistence.bean.PeticionBean;
import featurerequest.persistence.bean.UsuarioBean;

public interface PeticionDAO {
	public List<PeticionBean> buscarPeticiones(UsuarioBean solicitante);

	public void guardarPeticion(PeticionBean peticion);

	public void borrarPeticion(PeticionBean peticion);

}
