package featurerequest.persistence.dao;

import java.util.List;


import featurerequest.persistence.bean.ComentarioBean;
import featurerequest.persistence.bean.PeticionBean;

public interface ComentarioDAO {
	public List<ComentarioBean> buscarComentariosPorPeticion(PeticionBean peticion);

	public void guardarComentario(ComentarioBean comentario);

}
