package featurerequest.persistence.dao.hibernate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import featurerequest.persistence.bean.PeticionBean;
import featurerequest.persistence.bean.UsuarioBean;
import featurerequest.persistence.dao.PeticionDAO;

public class PeticionDAOHibernate implements PeticionDAO {
	private Session ses;

	public PeticionDAOHibernate(Session ses) {
		this.ses = ses;
	}

	@Override
	public List<PeticionBean> buscarPeticiones(UsuarioBean solicitante) {
		List<PeticionBean> listaPeticiones = null;
		try {
			listaPeticiones = (List<PeticionBean>) ses.createQuery("from PeticionBean where id_usuario=:id_usuario").setLong("id_usuario", solicitante.getId())
					.list();
		} catch (HibernateException he) {
			System.out.println(he.getMessage());
		}
		if (listaPeticiones == null) {
			listaPeticiones = new ArrayList<PeticionBean>();
		}
		return listaPeticiones;
	}

	@Override
	public void guardarPeticion(PeticionBean peticion) {
		try {
			ses.saveOrUpdate(peticion);
		} catch (HibernateException he) {
			System.out.println(he.getMessage());
		}
	}

	@Override
	public void borrarPeticion(PeticionBean peticion) {
		try {
			ses.delete(peticion);
		} catch (HibernateException he) {
			System.out.println(he.getMessage());
		}

	}

}
