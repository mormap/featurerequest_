package featurerequest.persistence.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import featurerequest.persistence.bean.ComentarioBean;
import featurerequest.persistence.bean.PeticionBean;
import featurerequest.persistence.dao.ComentarioDAO;

public class ComentarioDAOHibernate implements ComentarioDAO {
	private Session ses;

	public  ComentarioDAOHibernate(Session ses) {
		this.ses = ses;
	}

	@Override
	public List<ComentarioBean> buscarComentariosPorPeticion(PeticionBean peticion) {
		List<ComentarioBean> listaComentarios = null;
		try {
			listaComentarios = (List<ComentarioBean>) ses.createQuery("from ProductoBean").setLong("id_peticion", peticion.getId()).list();
		} catch (HibernateException he) {
			System.out.println(he.getMessage());
		}
		if (listaComentarios == null) {
			listaComentarios = new ArrayList<ComentarioBean>();
		}
		return listaComentarios;
	}

	@Override
	public void guardarComentario(ComentarioBean comentario) {
		try {
			ses.delete(comentario);
		} catch (HibernateException he) {
			System.out.println(he.getMessage());
		}
	}

}
