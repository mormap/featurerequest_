package featurerequest.persistence.dao.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import featurerequest.persistence.bean.UsuarioDesarrolladorBean;
import featurerequest.persistence.dao.UsuarioDesarrolladorDAO;

public class UsuarioDesarrolladorDAOHibernate implements UsuarioDesarrolladorDAO {
	private Session ses;

	public UsuarioDesarrolladorDAOHibernate(Session ses) {
		this.ses = ses;
	}

	@Override
	public UsuarioDesarrolladorBean buscarUsuario(String email, String password) {
		UsuarioDesarrolladorBean user = null;
		try {
			user = (UsuarioDesarrolladorBean) ses
					.createQuery("FROM UsuarioDesarrolladorBean Usuario where Usuario.email = :email and Usuario.password = :password")
					.setString("email", email).setString("password", password).uniqueResult();
		} catch (HibernateException e) {
			System.out.println("ERROR!!! " + e.getMessage());
		}
		return user;
	}

}
