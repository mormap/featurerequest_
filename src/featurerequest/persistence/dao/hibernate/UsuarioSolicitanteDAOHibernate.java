package featurerequest.persistence.dao.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import featurerequest.persistence.bean.UsuarioSolicitanteBean;
import featurerequest.persistence.dao.UsuarioSolicitanteDAO;

public class UsuarioSolicitanteDAOHibernate implements UsuarioSolicitanteDAO {
	private Session ses;

	public UsuarioSolicitanteDAOHibernate(Session ses) {
		this.ses = ses;
	}

	public UsuarioSolicitanteBean buscarUsuario(String email, String password) {

		UsuarioSolicitanteBean user = null;
		try {

			user = (UsuarioSolicitanteBean) ses.createQuery("FROM UsuarioSolicitanteBean usuario where usuario.email=:email and password=:password")
					.setString("email", email).setString("password", password).uniqueResult();

		} catch (HibernateException e) {
			System.out.println("ERROR!!! " + e.getMessage());
		}
		return user;
	}
}
