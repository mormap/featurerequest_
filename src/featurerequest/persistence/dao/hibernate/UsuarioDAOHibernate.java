package featurerequest.persistence.dao.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;


import featurerequest.persistence.dao.UsuarioDAO;

public class UsuarioDAOHibernate implements UsuarioDAO {
	private Session ses;

	public  UsuarioDAOHibernate(Session ses) {
		this.ses = ses;
	}
	public int getRol(String email, String password) {
		int rol = 0;
		try {
			rol = (Integer) ses.createQuery("SELECT Usuario.tipo FROM UsuarioBean Usuario where Usuario.email = :email and Usuario.password = :password")
					.setString("email", email)
					.setString("password",password)
					.uniqueResult();
		} catch (HibernateException e) {
			System.out.println("ERROR!!! " + e.getMessage());
		}
		return rol;
	}
}
