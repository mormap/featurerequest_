package featurerequest.persistence.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import featurerequest.persistence.bean.ProductoBean;
import featurerequest.persistence.bean.UsuarioBean;
import featurerequest.persistence.dao.ProductoDAO;

public class ProductoDAOHibernate implements ProductoDAO {
	private Session ses;

	public ProductoDAOHibernate(Session ses) {
		this.ses = ses;
	}

	@Override
	public List<ProductoBean> buscarProductos(UsuarioBean desarrollador) {
		List<ProductoBean> listaProductos = null;
		try {
			listaProductos = (List<ProductoBean>) ses.createQuery("from ProductoBean").setLong("id_usuario", desarrollador.getId()).list();
		} catch (HibernateException he) {
			System.out.println(he.getMessage());
		}
		if (listaProductos == null) {
			listaProductos = new ArrayList<ProductoBean>();
		}
		return listaProductos;
	}

	@Override
	public void guardarProducto(ProductoBean producto) {
		try {
			ses.saveOrUpdate(producto);
		} catch (HibernateException he) {
			System.out.println(he.getMessage());
		}
	}

	@Override
	public void borrarProducto(ProductoBean producto) {
		try {
			ses.delete(producto);
		} catch (HibernateException he) {
			System.out.println(he.getMessage());
		}

	}

}
