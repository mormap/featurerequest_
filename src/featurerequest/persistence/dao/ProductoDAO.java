package featurerequest.persistence.dao;

import java.util.List;

import featurerequest.persistence.bean.ProductoBean;
import featurerequest.persistence.bean.UsuarioBean;

public interface ProductoDAO {
	public List<ProductoBean> buscarProductos(UsuarioBean desarrollador);

	public void guardarProducto(ProductoBean producto);

	public void borrarProducto(ProductoBean producto);

}
